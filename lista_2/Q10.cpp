#include <iostream>
using namespace std;

int main(){
  int frango_disp, bife_disp, massa_disp, frango_sol, bife_sol, massa_sol, total1, total2, total3;
  cin>>frango_disp>>bife_disp>>massa_disp;
  cin>>frango_sol>>bife_sol>>massa_sol;

  total1 = (frango_disp < frango_sol) ? frango_sol - frango_disp : 0;
  total2 = (bife_disp < bife_sol) ? bife_sol - bife_disp : 0;
  total3 = (massa_disp < massa_sol) ? massa_sol - massa_disp : 0;

  cout<<total1 + total2 + total3<<endl;
}
