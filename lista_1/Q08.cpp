/*
Leia um valor inteiro correspondente à idade de uma pessoa em dias e informe-a em anos, meses e dias

  Obs.: apenas para facilitar o cálculo, considere todo ano com 365 dias e todo mês com 30 dias. Nos casos de teste nunca haverá uma situação que permite 12 meses e alguns dias, como 360, 363 ou 364. Este é apenas um exercício com objetivo de testar raciocínio matemático simples.

  Entrada
  O arquivo de entrada contém um valor inteiro.

  Saída
  Imprima a saída conforme exemplo fornecido.
*/

#include <iostream>
using namespace std;

int main()
{
	int number;
	cin>>number;
	const int y = 365;
	const int m = 30;
	int years = number/y;
	int months = number%y/m;
	int days = (number%y)%m;

	cout<<years<<" ano(s)"<<endl;
	cout<<months<<" mes(es)"<<endl;
	cout<<days<<" dia(s)"<<endl;
}
