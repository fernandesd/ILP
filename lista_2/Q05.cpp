/*
Leia 3 valores de ponto flutuante e efetue o cálculo das raízes da equação de Bhaskara. Se não for possível calcular as raízes, mostre a mensagem correspondente “Impossivel calcular”, caso haja uma divisão por 0 ou raiz de numero negativo.

Entrada
Leia três valores de ponto flutuante (double) A, B e C.

Saída
Se não houver possibilidade de calcular as raízes, apresente a mensagem "Impossivel calcular". Caso contrário, imprima o resultado das raízes com 5 dígitos após o ponto, com uma mensagem correspondente conforme exemplo abaixo. Imprima sempre o final de linha após cada mensagem.
*/
#include <iostream>
#include <iomanip>
#include <math.h>
using namespace std;
int main()
{
  double A, B, C, delta;
  cin>>A>>B>>C;
  cout<<fixed<<setprecision(5);

  delta = pow(B, 2) - 4*A*C;

  if (!((A == 0)||(delta < 0))){
    cout<<"R1 = "<<(-B+sqrt(delta))/(2*A)<<endl;
    cout<<"R2 = "<<(-B-sqrt(delta))/(2*A)<<endl;
  }else{
    cout<<"Impossivel calcular\n";
  }
}
