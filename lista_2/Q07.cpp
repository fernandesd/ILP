#include <iostream>
#include <math.h>
using namespace std;

int main(){
  double A, B, C, MIN, MAX, MID, POW_MAX, POW_MID, POW_MIN;
  cin>>A>>B>>C;

  if ((A >= B) && (B >= C)){
    MAX = A;
    MID = B;
    MIN = C;
  } else if ((A > B) && (C > B)){
    MAX = A;
    MID = C;
    MIN = B;
  }

  if ((B >= A) && (A >= C)){
    MAX = B;
    MID = A;
    MIN = C;
  } else if ((B > A) && (C > A)){
    MAX = B;
    MID = C;
    MIN = A;
  }

  if ((C >= A) && (A >= B)){
    MAX = C;
    MID = A;
    MIN = B;
  } else if ((C > A) && (B > A) && (C > B)){
    MAX = C;
    MID = B;
    MIN = A;
  } else if ((A == B) && (B == C)){
    MAX = A;
    MID = B;
    MIN = C;
  }

  POW_MAX = pow (MAX, 2.0);
  POW_MID = pow (MID, 2.0);
  POW_MIN = pow (MIN, 2.0);

  if (MAX >= MID + MIN){
    cout<<"NAO FORMA TRIANGULO\n";
  }else{

  if (POW_MAX == POW_MID + POW_MIN){
    cout<<"TRIANGULO RETANGULO\n";
  }

  if (POW_MAX > POW_MID + POW_MIN){
    cout<<"TRIANGULO OBTUSANGULO\n";
  }

  if (POW_MAX < POW_MID + POW_MIN){
    cout<<"TRIANGULO ACUTANGULO\n";
  }

  if ((A == B) && (B == C)){
    cout<<"TRIANGULO EQUILATERO\n";
  }

  if ( ((A == B) && (A != C)) || ((B == C) && (B != A)) || ((A == C) && (C != B)) ){
    cout<<"TRIANGULO ISOSCELES\n";
  }

  }

}
